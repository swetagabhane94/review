fetch("https://restcountries.com/v3.1/all")
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    console.log("Data fetch");

    myCountry(data);
  })
  .catch((err) => {
    console.error(`error occured on fetching file ${err}`);
  });

function myCountry(detail) {
  const loader = document.querySelector(".loader");
  loader.style.display = "none";

  detail.forEach((element) => {
    const ul = document.querySelector("#country-data");
    const list = document.createElement("li");
    const image = document.createElement("img");
    image.src = element.flags.png;

    const countryName = document.createElement("h3");
    countryName.textContent = element.name.common;
    list.append(image, countryName);
    ul.append(list);
  });
}
